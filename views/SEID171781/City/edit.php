<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\City\City();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>City Add Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">


    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Cities Information Entry</h1>

    <form action="update.php" method="post">

        <strong>Please Enter The City Name:</strong>

        <input type="text" name="name" value ="<?php  echo $singleData->name ?>">
        <br>

        <strong>Please Select The City Name:</strong>
        <label for="cities" ></label>
        <br>
        <select class="form-control" name="city" id="cities" >
            <br>

            <option selected ="selected" name="city" value="Dhaka" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Dhaka
            <option selected ="selected" name="city" value="Chittagong" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Chittagong
            <option selected ="selected" name="city" value="Barishal" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Barishal
            <option selected ="selected" name="city" value="Khulna" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Khulna
            <option selected ="selected" name="city" value="Sylhet" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Sylhet
            <option selected ="selected" name="city" value="Rangpur" <?php if($singleData->city == "Dhaka") echo 'selected="selected" '?>> Rangpur
            </select>

        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

        <input type="submit" value="Update">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
