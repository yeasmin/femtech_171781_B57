
<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;

    $obj = new\App\City\City();
    $allData = $obj->trashed();

     $msg = Message::message();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<?php
      echo "<div> <div id = 'message'>$msg</div> </div>"

?>

<h1> Active List of - Cities </h1>

<table class="table table-bordered table-striped">
    <tr>

        <th>Serial</th>
        <th>ID</th>
        <th>City Name</th>
        <th>City</th>
        <th>Action Buttons</th>
    </tr>

    <?php

    $serial = 1;

    foreach($allData as $row){
        echo "
        
        <tr>
        <td>$serial</td>
        <td>$row->id</td>
        <td>$row->name</td>
        <td>$row->city</td>
        <td>  
        <a href='view.php?id=$row->id'><button class='btn btn-primary'> View </button></a>
        <a href='edit.php?id=$row->id'><button class='btn btn-info'> Edit </button></a>
        <a href='recover.php?id=$row->id'><button class='btn btn-success'> Recover </button></a>
        <a href='delete.php?id=$row->id' onclick='return confirm_delete()'><button class='btn btn-danger'> Delete </button></a>
        </td>
        </tr>
        
        ";
        $serial++;


    }


    ?>
</table>


<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

<script type="text/javascript">
    function confirm_delete() {
        return confirm('Are you sure?');
    }


</script>

</body>
</html>












