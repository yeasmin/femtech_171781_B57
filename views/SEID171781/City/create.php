<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>City Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">City Information Entry</h1>
    <form action="store.php" method="post">
        <strong>Please Enter City Name:</strong>
        <input type="text" name="name" placeholder="Enter City Name">
        <br>
        <label for="cities" >Select list (select one):</label>
        <select class="form-control" name="city" id="cities" >

            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Barishal">Barishal</option>
            <option value="Khulna">Khulna</option>
            <option value="Sylhet">Sylhet</option>
            <option value="Rangpur">Rangpur</option>
        </select><br>

        <input type="submit" value="Submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
