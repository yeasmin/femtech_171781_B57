<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Create Form</title>

    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background: skyblue;
        }

    </style>
</head>
<body>

<div class="container">
    <div style="beight: 80px"></div>
    <h2 style="color: #2b2b2b">Input Name and Hobbies:</h2>

    <form class="form-group" action="store.php" method="post">


        <label>Name:</label>
        <input class="form-control col-sm-8" type="text" name="name" placeholder="Enter name">
        <br>
        <label>Hobbies:</label>

        <input type="checkbox" name="Hobbies[0]" value="Gardening">Gardening
        <input type="checkbox" name="Hobbies[1]" value="Programming">Programming
        <input type="checkbox" name="Hobbies[2]" value="Traveling">Traveling
        <input type="checkbox" name="Hobbies[3]" value="Swimming">Swimming
        <input type="checkbox" name="Hobbies[4]" value="Gaming">Gaming
        <br>

        <input type="submit" class="btn btn-primary" value="Submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
