<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\Hobbies\Hobbies();

$obj->setData($_GET);

$singleData = $obj->view();
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Create Form</title>

    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background: skyblue;
        }

    </style>
</head>
<body>

<div class="container">
    <div style="beight: 80px"></div>
    <h2 style="color: #2b2b2b">Input Name and Hobbies:</h2>

    <form class="form-group" action="update.php" method="post">

        <input type="hidden" id= "id" name="id" class="form-group" value="<?php echo $singleData->id?>">
        <div class="form-group">

        <label for="name">Name:</label>
        <input class="form-control col-sm-8" type="text" id= "id" name="name" placeholder="Enter name" value="<?php echo $singleData->name?>">
        </div>
        <br>
        <div class="form-group">
        <label for="hobbies">Hobbies:</label>

        <input type="checkbox" id= "hobbies" name="Hobbies[0]" value="Gardening"<?php if($singleData->hobbies == "Gardening") echo 'checked="checked" '?>> Gardening
        <input type="checkbox" id= "hobbies" name="Hobbies[1]" value="Programming"<?php if($singleData->hobbies == "Programming") echo 'checked="checked" '?>> Programming
        <input type="checkbox" id= "hobbies" name="Hobbies[2]" value="Traveling"<?php if($singleData->hobbies == "Traveling") echo 'checked="checked" '?>> Traveling
        <input type="checkbox" id= "hobbies" name="Hobbies[3]" value="Swimming"<?php if($singleData->hobbies == "Swimming") echo 'checked="checked" '?>> Swimming
        <input type="checkbox" id= "hobbies" name="Hobbies[4]" value="Gaming"<?php if($singleData->hobbies == "Gaming") echo 'checked="checked" '?>> Gaming
        </div>
        <br>



        <input type="Submit" class="btn btn-primary" value="Update">

    </form>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
