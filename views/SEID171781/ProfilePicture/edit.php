<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\ProfilePicture\ProfilePicture();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Profile Picture Entry</h1>

    <form class="form-group f" action="update.php" method="post" enctype="multipart/form-data">
        <strong>Please Enter Person's Name:</strong>
        <br>
        <input class="form-control" type="text" name="name" value="<?php echo $singleData->name?>">
        <br>

        <strong>Enter Person's Profile Picture:</strong>
        <br>
        <input type="file" name="image" accept=".png, .jpg, .jpeg ">
        <img src="images/<?php echo $singleData->profile_name?>" alt="image" height="300px" width="300px" class="img-responsive">
        <br>


        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

        <input type="Submit" value="Update">

    </form>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
