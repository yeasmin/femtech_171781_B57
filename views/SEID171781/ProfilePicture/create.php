<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Profile Picture Entry</h1>

    <form class="form-group f" action="store.php" method="post" enctype="multipart/form-data">
        <strong>Please Enter Person's Name:</strong>
        <br>
        <input class="form-control" type="text" name="name">
        <br>

        <strong>Enter Person's Profile Picture:</strong>
        <br>
        <input type="file" name="image" accept=".png, .jpg, .jpeg ">

        <br>

        <input type="Submit" value="Submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
