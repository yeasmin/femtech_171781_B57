<?php
if (isset($_SERVER['HTTP_REFERER']))
    $path = $_SERVER['HTTP_REFERER'];

require_once("../../../vendor/autoload.php");

use App\ProfilePicture\ProfilePicture;
use App\Utility\Utility;

$obj = new ProfilePicture();
$obj->setData($_POST);
$singleData = $obj-> view();
$fileName = $singleData->profile_name;

if ($_FILES['image'] ['name']!="") {
    $fileName = time() . $_FILES['image'] ['name'];

    $source = $_FILES['image'] ['tmp_name'];

    $destination = "Images/" . $fileName;

    move_uploaded_file($source, $destination);
}
$_POST['profilePicture'] =$fileName;

$obj->setData($_GET);

$obj->delete();

Utility::redirect($path);