
<?php
use App\Utility\Utility;
use App\Message\Message;

require_once ("../../../vendor/autoload.php");

if(!isset($_GET['id'])){
    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");
}
$obj = new\App\BookTitle\BookTitle();
$obj->setData($_GET);
$singleData = $obj->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        <title>Document</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<h1> Single Book Information Book Title </h1>

<table class="table table-bordered table-striped">

    <?php




    echo "

            <tr><td>ID:</td> <td>$singleData->id</td></tr>
            <tr><td>Book Title:</td> <td>$singleData->book_title</td></tr>
            <tr> <td>Author:</td> <td>$singleData->author_name</td></tr>

            ";

    ?>

</table>

</body>
</html>
