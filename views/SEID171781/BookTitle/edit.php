<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\BookTitle\BookTitle();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">


    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Book Information Entry</h1>

    <form action="update.php" method="post">

        <strong>Please Enter Book Title:</strong>

        <input type="text" name="bookTitle" value ="<?php  echo $singleData->book_title ?>">
        <br>

        <strong>Please Enter Author Name:</strong>
        <input type="text" name="authorName" value ="<?php  echo $singleData->author_name ?>">
        <br>

        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

        <input type="submit" value="Update">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
