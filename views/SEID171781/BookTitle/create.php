<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Book Information Entry</h1>
    <form action="store.php" method="post">
        <strong>Please Enter Book Title:</strong>
        <input type="text" name="bookTitle">
        <br>

        <strong>Please Enter Author Name:</strong>
        <input type="text" name="authorName">
        <br>

        <input type="submit" value="Submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
