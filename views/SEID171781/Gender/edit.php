<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\Gender\Gender();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <title>Gender Edit</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <style>
        div.container{
            margin-top: 170px;
        }
        div.col-lg-6{
            background-color: #ebccd1;
            border-radius: 10px;
        }
        body{
            background: skyblue;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    <h2>Gender Edit Form</h2>

    <form class="form-horizontal" action ="update.php" method ="post">

        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

                <div class="form-group">

                  <label class="control-label col-sm-3" for="name">Name</label>
                  <div class="col-sm-9">
                      <input type="text"  class="form-control" name="name" value="<?php echo $singleData->name?>">

                </div>
               </div>


                   <div class="form-group">
                       <label class="control-label col-sm-3" for="gender">Gender</label>
                       <div class="col-sm-9">
                           <input type="radio" name="gender" value="Male" <?php if($singleData->gender == "Male") echo 'checked="checked" '?>> Male
                           <input type="radio" name="gender" value="Female" <?php if($singleData->gender == "Female") echo 'checked="checked" '?>> Female
                       </div>
                 C

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn-btn-success">Update</button>

                </div>
           </div>

    </form>
</div>
<div class="col-lg-3"></div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
