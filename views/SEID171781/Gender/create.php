<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <title>Gender Information</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <style>
        div.container{
            margin-top: 170px;
        }
        div.col-lg-6{
            background-color: #ebccd1;
            border-radius: 10px;
        }
        body{
            background: skyblue;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <h2>Gender Form</h2>

        <form class="form-group" action ="store.php" method ="post">
        <div><input type="text"  class="form-control" name="name" placeholder="Enter your name"></div>
            <div class="radio">
                <label><input type="radio" name="gender" value="male" >Male</label><br>
                <label><input type="radio" name="gender" value="female" >Female</label>
            </div>
        <div><input type="submit" value="Add"></div>
    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
