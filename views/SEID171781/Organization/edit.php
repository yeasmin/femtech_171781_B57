<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\Organization\Organization();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Organization Summary Edit</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1>Organization Summary Edit</h1>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label >Enter Your Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter your name" value="<?php echo $singleData->name?>">
        </div>
        <div class="form-group">
            <label >Enter Organization Name:</label>
            <input type="text" class="form-control" name="organization" placeholder="Enter organization name" value="<?php echo $singleData->organization?>">
        </div>
        <div class="form-group">
            <label for="summary">Enter Organization Summary:</label>
            <textarea class="form-control" rows="5" name="summary" <?php echo $singleData->summary?> >
            </textarea>
</div>
        <input type="hidden" name="id"  value="<?php echo $singleData->id?>">
        <div><input type="submit" value="Update"></div>
    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
