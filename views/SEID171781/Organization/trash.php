<?php
require_once("../../../vendor/autoload.php");

use App\Organization\Organization;
use App\Utility\Utility;

$obj = new Organization();

$obj->setData($_POST);

$obj->trash();

Utility::redirect("trashed.php");