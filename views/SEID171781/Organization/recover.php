<?php
require_once("../../../vendor/autoload.php");

use App\Organization\Organization;
use App\Utility\Utility;

$obj = new Organization();

$obj->setData($_GET);

$obj->recover();

Utility::redirect("index.php");