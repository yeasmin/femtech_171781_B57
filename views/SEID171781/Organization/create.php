<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Organization Summary</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1>Add Organization Summary</h1>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label >Enter Your Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter your name">
        </div>
        <div class="form-group">
            <label >Enter Organization Name:</label>
            <input type="text" class="form-control" name="organization" placeholder="Enter organization name">
        </div>
        <div class="form-group">
            <label for="comment">Enter Organization Summary:</label>
            <textarea class="form-control" rows="5" id="comment"></textarea>
        </div>
        <div><input type="submit" value="Submit"></div>
    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
