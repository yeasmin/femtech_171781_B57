<?php
require_once("../../../vendor/autoload.php");

use App\BirthDay\Birthday;
use App\Utility\Utility;

$obj = new Birthday();

$obj->setData($_POST);

$obj->store();

Utility::redirect("create.php");
