<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])){

    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");

}
$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";

$obj = new\App\Birthday\Birthday();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>BirthDay Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1>BirthDay Information</h1>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Please Enter Your Name:</label>
            <input type="text" class="form-control" id="name"  name="name" value="<?php echo $singleData->name ?>">
        </div>
        <br>
        <div class="form-group">

            <label>Please Enter Your Birth Date:</label>
            <input type="date" class="form-control" name="birthDate" placeholder="mm-dd-yyyy" value="<?php echo $singleData->birth_date ?>">
        </div>
        <br>
        <input type="hidden" name="id" value="<?php echo $singleData->id?>">
        <div><input type="submit" value="Update"></div>
    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
