
<?php
require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;



if(!isset($_GET['id'])){
    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");
}

$obj = new\App\BirthDay\BirthDay();
$obj->setData($_GET);
$singleData = $obj->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        <title>Document</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<h1> Single Birthday Information </h1>

<table class="table table-bordered table-striped">

    <?php




    echo "

            <tr><td>ID:</td> <td>$singleData->id</td></tr>
            <tr><td>Name:</td> <td>$singleData->name</td></tr>
            <tr> <td>Gender:</td> <td>$singleData->birth_date</td></tr>




            ";

    ?>

</table>

</body>
</html>
