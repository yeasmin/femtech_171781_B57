<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$message = Message::message();

echo "<div> <div id='message'>$message</div> </div>";
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Birthday Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">

    <style>
        body{
            background: skyblue;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 style="color: #2b2b2b">Birthday Information Entry</h1>
    <form role= "form" action="store.php" method="post">
        <div class="form-group">
        <label>Please Enter Your Name:</label>
        <input type="text" class="form-control"  name="name">
        </div>
        <br>
        <div class="form-group">

        <label>Please Enter Your Birth Date:</label>
        <input type="date" class="form-control" name="birthDate" placeholder="mm-dd-yyyy">
        </div>
        <br>

        <input type="submit" value="Submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery(
        function ($) {

            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);
            $('#message').fadeIn(550);
            $('#message').fadeOut(550);

        }
    )
</script>

</body>
</html>
