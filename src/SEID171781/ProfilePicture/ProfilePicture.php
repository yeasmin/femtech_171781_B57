<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 5/30/2017
 * Time: 11:06 PM
 */

namespace App\ProfilePicture;


use App\Model\Database;
use App\Message\Message;
use PDO;

class ProfilePicture extends Database
{
    public $id;
    public $name;
    public $profilePicture;

    public function setData($postArray){
        if (array_key_exists("id",$postArray))
        {
            $this->id= $postArray['id'];
        }
        if (array_key_exists("name",$postArray))
        {
            $this->name=$postArray['name'];
        }
        if (array_key_exists("profilePicture",$postArray))
        {
            $this->profilePicture=$postArray['profilePicture'];
        }
    }

    public function store(){


        $name = $this->name;
        $profilePicture= $this->profilePicture;

        $sqlQuery= "INSERT INTO `profile_pictures` (`name`, `profile_name`) VALUES ( ?, ?);";
        $dataArray= array($name, $profilePicture);

        $STH = $this->DBH->prepare($sqlQuery);

        $result= $STH->execute($dataArray);
        if ($result){
            Message::message("Data has been inserted successfully.");
        }
        else{
            Message::message("Data has not been inserted due to an error.");
        }


    }
    public function index()
    {

        $sqlQuery = "Select * from profile_pictures";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function view()
    {

        $sqlQuery = "Select * from profile_pictures where id=" . $this->id;

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();
        return $singleData;
    }
    public function update()
    {

        $sqlQuery = "UPDATE `profile_pictures` SET `name` = ?, `profile_name` = ? WHERE `id` = $this->id;";

        $dataArray = array($this->name, $this->profilePicture);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :( Data has not been updated due to an error.");

        }
    }
    public function trashed()
    {

        $sqlQuery = "Select * from profile_name where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `profile_name` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `profile_name` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `profile_name` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }
}
