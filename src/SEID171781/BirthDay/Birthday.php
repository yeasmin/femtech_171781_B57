<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 5/31/2017
 * Time: 3:07 AM
 */

namespace App\Birthday;

use PDO;
use App\Model\Database;
use App\Message\Message;

class Birthday extends Database
{
    public $id;
    public $name;
    public $birthDate;

    public function setData($postArray)
    {
        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }
        if (array_key_exists("name", $postArray)) {
            $this->name = $postArray['name'];
        }
        if (array_key_exists("birthDate", $postArray)) {
            $this->birthDate = $postArray['birthDate'];
        }
    }

    public function store()
    {

        $sqlQuery = "INSERT INTO `birthday` (`name`, `birth_date`) VALUES ( ?, ?);";
        $dataArray = array($this->name, $this->birthDate);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);
        if ($result) {
            Message::message("Data has been inserted successfully.");
        } else {
            Message::message("Data has not been inserted due to an error.");
        }

    }

    public function index()
    {

        $sqlQuery = "Select * from birthday";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view()
    {

        $sqlQuery = "Select * from birthday where id=" . $this->id;

        $STH = $this->DBH->query($sqlQuery);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();
        return $singleData;

    }

    public function update()
    {

        $sqlQuery = "UPDATE `birthday` SET `name` = ?, `birth_date` = ? WHERE `id` = $this->id;";

        $dataArray = array($this->name, $this->birthDate);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :( Data has not been updated due to an error.");

        }
    }
    public function trashed()
    {

        $sqlQuery = "Select * from birthday where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `birthday` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `birthday` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `birthday` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }


}