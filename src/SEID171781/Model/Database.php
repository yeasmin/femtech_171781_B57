<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 5/30/2017
 * Time: 5:17 PM
 */

namespace App\Model;

use PDOException, PDO;
class Database
{
     public $DBH;

     public function __construct()
     {
         try{
             $this->DBH= new PDO("mysql:host=localhost;dbname=atomic_project_b57", "root", "");
             //echo "Database Connection Successful.";

         }
         catch (PDOException $error){
             echo $error->getMessage();

         }
     }
}