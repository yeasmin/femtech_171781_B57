<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 5/29/2017
 * Time: 12:12 PM
 */

namespace App\BookTitle;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class BookTitle extends Database
{
    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }

        if (array_key_exists("bookTitle", $postArray)) {
            $this->bookTitle = $postArray['bookTitle'];
        }

        if (array_key_exists("authorName", $postArray)) {
            $this->authorName = $postArray['authorName'];
        }
    }


    public function store()
    {

        $sqlQuery = "INSERT INTO `book_title` ( `book_title`, `author_name`) VALUES ( ?, ?);";
        $dataArray = array($this->bookTitle, $this->authorName);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been inserted successfully.");
        } else {
            Message::message("Failure :( Data has not been inserted due to an error.");

        }

    }


    public function index()
    {

        $sqlQuery = "Select * from book_title where is_trashed= 'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }


    public function view()
    {

        $sqlQuery = "Select * from book_title where id=" . $this->id;

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();
        return $singleData;
    }

    public function update()
    {

        $sqlQuery = "UPDATE `book_title` SET `book_title` = ?, `author_name` = ? WHERE `id` = $this->id;";

        $dataArray = array($this->bookTitle, $this->authorName);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :( Data has not been updated due to an error.");

        }

    }


    public function trashed()
    {

        $sqlQuery = "Select * from book_title where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `book_title` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `book_title` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `book_title` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }
}



