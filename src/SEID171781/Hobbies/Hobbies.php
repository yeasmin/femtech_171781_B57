<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database;
use PDO;
class Hobbies extends Database
{
    public $id;
    public $name;
    public $hobbies;

    public function setData($postArray){
        if (array_key_exists("id",$postArray))
        {
            $this->id= $postArray['id'];
        }
        if (array_key_exists("name",$postArray))
        {
            $this->name=$postArray['name'];
        }
        if (array_key_exists("hobbies",$postArray))
        {
            $this->hobbies=$postArray['hobbies'];
        }
    }

    public function store(){


        $sqlQuery= "INSERT INTO `hobbies` (`name`, `hobbies`) VALUES ( ?, ?);";
        $dataArray= array($this->name, $this->hobbies);

        $STH = $this->DBH->prepare($sqlQuery);
        $result= $STH->execute($dataArray);
        if ($result){
            Message::message("Data has been inserted successfully.");
        }
        else{
            Message::message("Data has not been inserted due to an error.");
        }

    }
    public function index(){

        $sqlQuery = "Select * from hobbies";

        $STH =$this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function view(){

        $sqlQuery = "Select * from hobbies where id=".$this->id;

        $STH =$this->DBH->query($sqlQuery);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update()
    {

        $sqlQuery = "UPDATE `hobbies` SET `name` = ?, `hobbies` = ? WHERE `id` = $this->id;";

        $dataArray = array($this->name, $this->hobbies);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :) Data has not been updated due to an error.");

        }
    }
    public function trashed()
    {

        $sqlQuery = "Select * from hobbies where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `hobbies` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `hobbies` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `hobbies` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }

}