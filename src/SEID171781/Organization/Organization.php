<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 5/31/2017
 * Time: 10:25 AM
 */

namespace App\Organization;

use PDO;
use App\Model\Database;
use App\Message\Message;
class Organization extends Database
{
    public $id;
    public $name;
    public $organization;

    public function setData($postArray){
        if (array_key_exists("id",$postArray))
        {
            $this->id= $postArray['id'];
        }
        if (array_key_exists("name",$postArray))
        {
            $this->name=$postArray['name'];
        }
        if (array_key_exists("organization",$postArray))
        {
            $this->organization=$postArray['organization'];
        }
    }

    public function store(){


        $sqlQuery= "INSERT INTO `organizations` (`name`, `organization`) VALUES ( ?, ?);";
        $dataArray= array($this->name, $this->organization);

        $STH = $this->DBH->prepare($sqlQuery);
        $result= $STH->execute($dataArray);
        if ($result){
            Message::message("Data has been inserted successfully.");
        }
        else{
            Message::message("Data has not been inserted due to an error.");
        }

    }
    public function index(){

        $sqlQuery = "Select * from organizations";

        $STH =$this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function view(){

        $sqlQuery = "Select * from organizations where id=".$this->id;

        $STH =$this->DBH->query($sqlQuery);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update()
    {

        $sqlQuery = "UPDATE `organizations` SET `name` = ?, `organization` = ? WHERE `id` = $this->id;";

        $dataArray = array($this->name, $this->organization);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success :) Data has been updated successfully.");
        } else {
            Message::message("Failure :) Data has not been updated due to an error.");

        }
    }
    public function trashed()
    {

        $sqlQuery = "Select * from organizations where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `organizations` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `organizations` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `organizations` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }
}