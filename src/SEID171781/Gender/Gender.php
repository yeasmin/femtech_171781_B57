<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 5/31/2017
 * Time: 9:54 AM
 */

namespace App\Gender;

use PDO;
use App\Model\Database;
use App\Message\Message;

class Gender extends Database
{
    public $id;
    public $name;
    public $gender;

    public function setData($postArray){
        if (array_key_exists("id",$postArray))
        {
            $this->id= $postArray['id'];
        }
        if (array_key_exists("name",$postArray))
        {
            $this->name=$postArray['name'];
        }
        if (array_key_exists("gender",$postArray))
        {
            $this->gender=$postArray['gender'];
        }
    }

    public function store(){

        $sqlQuery= "INSERT INTO `genders` (`name`, `gender`) VALUES (?, ?);";
        $dataArray= array($this->name, $this->gender);

        $STH = $this->DBH->prepare($sqlQuery);
        $result= $STH->execute($dataArray);
        if ($result){
            Message::message("Data has been inserted successfully.");
        }
        else{
            Message::message("Data has not been inserted due to an error.");
        }

    }
    public function index(){

        $sqlQuery = "Select * from genders";

        $STH =$this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }


    public function view(){

        $sqlQuery = "Select * from genders where id=".$this->id;

        $STH =$this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update()
    {

        $sqlQuery = " UPDATE `genders` SET `name` = ?, `gender` = ? WHERE `id` = $this->id;";
        $dataArray = array($this->name, $this->gender);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);
        if ($result) {
            Message::message("Data has been updated successfully.");
        } else {
            Message::message("Data has not been updated due to an error.");
        }
    }
    public function trashed()
    {

        $sqlQuery = "Select * from genders where is_trashed <>'No'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function trash()
    {

        $sqlQuery = "UPDATE `genders` SET is_trashed=NOW() WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function recover()
    {

        $sqlQuery = "UPDATE `genders` SET is_trashed='No' WHERE `id` = $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been trashed successfully.");
        } else {
            Message::message("Failure : Data has not been trashed due to an error.");

        }

    }
    public function delete()
    {

        $sqlQuery = "DELETE from `genders` where `id`= $this->id;";

        $result = $this->DBH->exec($sqlQuery);


        if ($result) {
            Message::message("Success : Data has been deleted successfully.");
        } else {
            Message::message("Failure : Data has not been deleted due to an error.");

        }

    }
}